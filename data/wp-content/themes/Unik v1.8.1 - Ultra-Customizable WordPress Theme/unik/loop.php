<?php /*** The loop that displays posts.***/ 

$get_meta = get_post_custom($post->ID);
$blogLayout=  weblusive_get_option('blog_layout');
if (empty($blogLayout)) $blogLayout = 1;

//(isset($_GET['layout']))? $blogLayout=$_GET['layout'] : $blogLayout='';
//isset ($blogLayout) ? $blogLayout : $blogLayout=1;

$showdate = weblusive_get_option('blog_show_date'); 
$showcomments = weblusive_get_option('blog_show_comments'); 
$showauthor = weblusive_get_option('blog_show_author'); 
$showrmtext = weblusive_get_option('blog_show_rmtext'); 
$day = get_the_time('d'); $month = get_the_time('m'); $year = get_the_time('Y');
?>

<?php if ( ! have_posts() ) : ?>
	<div id="post-0" class="post box-section error404 not-found">
		<h1 class="entry-title"><?php _e( 'Not Found', 'unik' ); ?></h1>
		<div class="entry-content">
			<p><?php _e( 'Apologies, but no results were found for the requested archive. Perhaps searching will help find a related post.', 'unik' ); ?></p>
			<?php get_search_form(); ?>
		</div><!-- .entry-content -->
	</div><!-- #post-0 -->
<?php else:?>
<?php if($blogLayout == 2 ):?><div class="row"><?php endif?>
<?php while ( have_posts() ) : the_post(); ?>
	<?php $get_meta = get_post_custom($post->ID);		
	$mediatype = isset($get_meta["_blog_mediatype"]) ? $get_meta["_blog_mediatype"][0] : 'image';
	$videoId = isset($get_meta["_blog_video"]) ? $get_meta["_blog_video"][0] : ''; 
	$autoplay =  isset($get_meta["_blog_videoap"]) ? $get_meta["_blog_videoap"][0] : '0';
	$thumbnail = '';
	if ( has_post_thumbnail()) {
		$thumbnail  = get_the_post_thumbnail($post->ID, 'blog-list');
		//if (empty($thumbnail)) $thumbnail = '<img src="http://placehold.it/310x223.png" alt="" />';
	}
	else {
		$thumbnail =  '';
	}
	if($blogLayout==2) :?><div class="col-md-6 isotope-item"><?php endif?>
<!-- ************* POST FORMAT IMAGE ************** -->
<?php if ( ( function_exists( 'get_post_format' ) && 'image' == get_post_format( $post->ID ) )  ) : ?> 
	<article id="post-<?php the_ID();?>" <?php post_class('blog-post post-format-image'); ?>>
		<div class="post-box">
			<div class="post-format"><i class="fa fa-image"></i></div>
			<?php $meta = unik_meta($post); echo $meta; ?>
			<div class="post-gal" <?php if($showdate && $showauthor && $showcomments):?>style="width:100% !important"<?php endif?>>
				<a href="<?php the_permalink()?>"><?php echo $thumbnail; ?></a>
				<div class="hover-post">
					<a href="<?php the_permalink()?>"><?php echo (isset($showrmtext)&& !empty($showrmtext)) ? $showrmtext : __('Read More', 'unik');?></a>
				</div>
			</div>
			<?php if($blogLayout==2) :?><div class="clearfix"></div><?php endif?>
			<div class="post-content">
				<h2><a href="<?php the_permalink()?>"><?php the_title()?></a></h2>
				<p><?php echo do_shortcode(get_the_excerpt()); ?></p>
			</div>
		</div>
	</article>
<!-- ************* POST FORMAT LINK ************** -->
<?php elseif( ( function_exists( 'get_post_format' ) && 'link' == get_post_format( $post->ID ) )  ) : ?> 

<div id="post-<?php the_ID();?>" <?php post_class('blog-post post-format-link'); ?>>
	<article class="post-box">
		<div class="post-format"><i class="fa fa-external-link-square"></i></div>
		<?php if($blogLayout==2) :?><div class="clearfix"></div><?php endif?>
		<div class="post-content">
			<?php $link = isset($get_meta["_blog_link"]) ? $get_meta["_blog_link"][0] : ''; ?>
			<h2><a href="<?php echo $link ?>"><?php the_title(); ?></a></h2>
		</div>
	</article>
</div>
<!-- ************* POST FORMAT AUDIO ************** -->
<?php elseif( ( function_exists( 'get_post_format' ) && 'audio' == get_post_format( $post->ID ) )  ) : ?> 
	<article id="post-<?php the_ID();?>" <?php post_class('blog-post post-format-audio'); ?>>
		<div class="post-box">
			<div class="post-format"><i class="fa fa-music"></i></div>
			<?php if($blogLayout==2) :?><div class="clearfix"></div><?php endif?>
			<div class="post-content">
				<h2><a href="<?php the_permalink()?>"><?php the_title()?></a></h2>
				<?php $audio = isset($get_meta["_blog_audio"]) ? $get_meta["_blog_audio"][0] : ''; ?>
				<?php if ($audio):?>
					<?php echo do_shortcode($audio); ?>
				<?php else:?>
					<div class="alert alert-danger fade in">
						<?php _e('Audio post format was chosen but no embed code provided. Please fix this by providing it.', 'unik') ?>
					</div>
				<?php endif?>
			</div>
		</div>
	</article>
<!-- ************* POST FORMAT STATUS ************** -->
<?php elseif( ( function_exists( 'get_post_format' ) && 'status' == get_post_format( $post->ID ) )  ) : ?> 
	<article id="post-<?php the_ID();?>" <?php post_class('blog-post post-format-status'); ?>>
		<div class="post-box">
			<div class="post-format"><i class="fa fa-flag"></i></div>
			<?php if($blogLayout==2) :?><div class="clearfix"></div><?php endif?>
			<div class="post-content">
				<h2><a href="<?php the_permalink()?>"><?php the_title()?></a></h2>
				<?php
				var_dump($get_meta["_blog_status"][0]);
				$status = isset($get_meta["_blog_status"]) ? $get_meta["_blog_status"][0] : ''; ?>
				<?php if ($status):?>
					<?php echo $status; ?>
				<?php else:?>
					<div class="alert alert-danger fade in">
						<?php _e('Status post format was chosen but no status text provided. Please fix this by providing it.', 'unik') ?>
					</div>
				<?php endif?>
			</div>
		</div>
	</article>
<!-- ************* POST FORMAT QUOTE ************** -->
<?php elseif( ( function_exists( 'get_post_format' ) && 'quote' == get_post_format( $post->ID ) )  ) : ?> 
	<article id="post-<?php the_ID();?>" <?php post_class('blog-post post-format-quote'); ?>>	
		<div class="post-box">
			<div class="post-format"><i class="fa fa-quote-right"></i></div>
			<?php if($blogLayout==2) :?><div class="clearfix"></div><?php endif?>
			<div class="post-content">
				<?php $quote = isset($get_meta["_blog_quote"]) ? $get_meta["_blog_quote"][0] : ''; ?>
				<?php if ($quote):?>
					<?php
					echo htmlspecialchars_decode(do_shortcode($quote)); ?>
				<?php else:?>
					<div class="alert alert-danger fade in">
						<?php _e('Quote post format was chosen but no url or embed code provided. Please fix this by providing it.', 'unik') ?>
					</div>
				<?php endif?>
			</div>
		</div>
	</article>
<!-- ************* POST FORMAT VIDEO ************** -->
<?php elseif( ( function_exists( 'get_post_format' ) && 'video' == get_post_format( $post->ID ) )  ) : ?> 
	<article id="post-<?php the_ID();?>" <?php post_class('blog-post post-format-video'); ?>>
		<div class="post-box">
			<div class="post-format"><i class="fa fa-video-camera"></i></div>
			<?php $meta = unik_meta($post); echo $meta; ?>
			<div class="post-gal" <?php if($showdate && $showauthor && $showcomments):?>style="width:100% !important"<?php endif?>>
				<?php
				global $wp_embed;
				$post_embed = '';
				$video = isset($get_meta["_blog_video"]) ? $get_meta["_blog_video"][0] : ''; 
				$videoself = isset($get_meta["_blog_video_selfhosted"]) ? $get_meta["_blog_video_selfhosted"][0] : ''; 
				if ($video || $videoself): ?>
					<div class="flex-video">
						<?php
							if ($video):
								$post_embed = $wp_embed->run_shortcode('[embed width="310" height="223"]'.$video.'[/embed]'); 
							else:
								$post_embed = $wp_embed->run_shortcode($videoself);
							endif;
							echo $post_embed; 
						?>
					</div>	
				<?php else:?>
					<div class="alert alert-danger fade in">
						<?php _e('Video post format was chosen but no url or embed code provided. Please fix this by providing it.', 'unik') ?>
					</div>
				<?php endif?>
			</div>
			<?php if($blogLayout==2) :?><div class="clearfix"></div><?php endif?>
			<div class="post-content">
				<h2><a href="<?php the_permalink()?>"><?php the_title()?></a></h2>
				<p><?php echo do_shortcode(get_the_excerpt()); ?></p>
				<a href="<?php the_permalink()?>"><?php echo (isset($showrmtext)&& !empty($showrmtext)) ? $showrmtext : __('Read More', 'unik');?></a>
			</div>
		</div>
	</article>
<!-- ************* POST FORMAT GALLERY ************** -->
<?php elseif( ( function_exists( 'get_post_format' ) && 'gallery' == get_post_format( $post->ID ) )  ) : ?> 
	<article id="post-<?php the_ID();?>" <?php post_class('blog-post post-format-video'); ?>>
		<div class="post-box">
			<div class="post-format"><i class="fa fa-video-camera"></i></div>
			<?php $meta = unik_meta($post); echo $meta; ?>
			<div class="post-gal" <?php if($showdate && $showauthor && $showcomments):?>style="width:100% !important"<?php endif?>>
				<div class="flexslider flexpost">
					<ul class="slides">
						<?php 
						$argsThumb = array(
							'order'          => 'ASC',
							'post_type'      => 'attachment',
							'post_parent'    => $post->ID,
							'post_mime_type' => 'image',
							'post_status'    => null,
							//'exclude' => get_post_thumbnail_id()
						);
						$attachments = get_posts($argsThumb);
						if ($attachments) {
							foreach ($attachments as $attachment) {
								echo '<li><img src="'.wp_get_attachment_url($attachment->ID, 'full', false, false).'" alt="'.get_post_meta($attachment->ID, '_wp_attachment_image_alt', true).'" /></li>';
							}
						}?>
					</ul>
				</div>
			</div>
			<?php if($blogLayout==2) :?><div class="clearfix"></div><?php endif?>
			<div class="post-content">
				<h2><a href="<?php the_permalink()?>"><?php the_title()?></a></h2>
				<p><?php echo do_shortcode(get_the_excerpt()); ?></p>
				<a href="<?php the_permalink()?>"><?php echo (isset($showrmtext)&& !empty($showrmtext)) ? $showrmtext : __('Read More', 'unik');?></a>
			</div>
		</div>
	</article>
<!-- ************* POST FORMAT STANDARD ************** -->
<?php else: ?> 
	<article id="post-<?php the_ID();?>" <?php post_class('blog-post post-format-standard'); ?>>
		<div class="post-box">
			<div class="post-format"><i class="fa fa-comment"></i></div>
			<?php $meta = unik_meta($post); echo $meta; ?>
			<div class="post-gal" <?php if($showdate && $showauthor && $showcomments):?>style="width:100% !important"<?php endif?>>
				<a href="<?php the_permalink()?>"><?php echo $thumbnail; ?></a>
				<?php if( has_post_thumbnail() ) { ?><div class="hover-post">
					<a href="<?php the_permalink()?>"><?php echo (isset($showrmtext)&& !empty($showrmtext)) ? $showrmtext : __('Read More', 'unik');?></a>
				</div><?php } ?>
			</div>
			<?php if($blogLayout==2) :?><div class="clearfix"></div><?php endif?>
			<div class="post-content">
				<h2><a href="<?php the_permalink()?>"><?php the_title()?></a></h2>
				<p><?php echo do_shortcode(get_the_excerpt()); ?></p>
				<a href="<?php the_permalink()?>"><?php echo (isset($showrmtext)&& !empty($showrmtext)) ? $showrmtext : __('Read More', 'unik');?></a>
			</div>
		</div>
	</article>
<?php endif?>
<?php if($blogLayout==2) :?></div><?php endif; ?>
	<?php endwhile;?>
         <?php if($blogLayout == 2 ):?></div><?php endif?>
<?php endif; ?>
