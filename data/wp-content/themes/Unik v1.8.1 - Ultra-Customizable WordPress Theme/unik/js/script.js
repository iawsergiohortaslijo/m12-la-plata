/*jshint jquery:true */
/*global $:true */

var $ = jQuery.noConflict();

var DevSolutionSkill={init:function(e,t){this.diagram(e)},diagram:function(e){var t=e,n=160,r=160,i=n / 2,s=r / 2,o=jQuery("#"+e).data("forground")?jQuery("#"+e).data("forground"):"#fff",u=jQuery("#"+e).data("background")?jQuery("#"+e).data("background"):"#007aff",ho=jQuery("#"+e).data("border")?jQuery("#"+e).data("border"):"#dfdfdf",ho2=jQuery("#"+e).data("border")?jQuery("#"+e).data("border"):"#dfdfdf",f=13,how=1,l=jQuery("#"+e).data("percent"),c=19,h=jQuery("#"+e).data("fontsize")?jQuery("#"+e).data("fontsize"):"30px",p=jQuery("#"+e).data("percentsize")?jQuery("#"+e).data("percentsize"):"28px",d=jQuery("#"+e).data("fontcolor")?jQuery("#"+e).data("fontcolor"):"#cccccc",v=jQuery("#"+e).data("font")?jQuery("#"+e).data("font"):"Open Sans";if(l==100){c=45}else if(l<10){c=20}else if(l>100){alert("you can't set more then 100");return false}
	var m=Raphael(t,n,r);m.customAttributes.arc=function(e,t,n){var r=3.6*e,o=r==360?359.99:r,u=90;a=(u-o)*Math.PI / 180,b=u*Math.PI / 180,sx=i+n*Math.cos(b),sy=s-n*Math.sin(b),x=i+n*Math.cos(a),y=s-n*Math.sin(a),path=[["M",sx,sy],["A",n,n,0,+(o>180),1,x,y]];return{path:path,stroke:t}};var g=m.path().attr({arc:[100,o,70],"stroke-width":f-1});var ko=m.path().attr({arc:[100,ho,64],"stroke-width":how});var ko=m.path().attr({arc:[100,ho2,76],"stroke-width":how});var w=m.path().attr({arc:[l,u,70],"stroke-width":f});w.animate({transform:"r360"+","+80+","+80},2e3);var E=m.text(n,r,l).attr({font:h+" "+v,fill:d,cx:0,cy:0,x:i-10,y:s}).toFront();E.stop().animate({opacity:0},0,">",function(){this.attr({text:l}).animate({opacity:1},2e3,"<")});var S=m.text(n,r,"%").attr({font:p+" "+v,fill:d,cx:0,cy:0,x:i+c,y:s}).toFront();S.stop().animate({opacity:0},0,">",function(){this.attr({text:"%"}).animate({opacity:1},2e3,"<")})}}

$(document).ready(function($) {
	"use strict";
	/* global DevSolutionSkill: false */

	/*---------------- Breadcrumbs fix --------------------*/
	jQuery( '.page-tree a').each(function() {
		var parentTag = jQuery(this ).parent().get(0).tagName;
		if(parentTag!=='LI'){
			jQuery( this ).wrap('<li>');
		}
	});
	/*----------------PROMO HEAD--------------------*/
	var sliderBanner = $('#banner .headpromo');
	try{
		sliderBanner.bxSlider({
			auto: true,
			mode: 'vertical'
		});
	} catch(err) {
		//alert ('There was an issue displaying the slider');
	}
	// sticky header
	var headmar= $('.fixed-head').height()+20;
	$('.fixed-head > div').next('div').css('margin-top', headmar)


	/*-------------------------------------------------*/
	/* =  portfolio isotope
	 /*-------------------------------------------------*/

	var winDow = $(window);
	// Needed variables
	var $container=$('.portfolio-box');
	var $filter=$('.filter.non-paginated');

	try{
		$container.imagesLoaded( function(){
			$container.show();
			$container.isotope({
				filter:'*',
				layoutMode:'masonry',
				animationOptions:{
					duration:750,
					easing:'linear'
				}
			});

		});
	} catch(err) {
	}

	winDow.bind('resize', function(){
		var selector = $filter.find('a.active').attr('data-filter');

		try {
			$container.isotope({
				filter	: selector,
				animationOptions: {
					duration: 750,
					easing	: 'linear',
					queue	: false,
				}
			});
		} catch(err) {
		}
		return false;
	});

	// Isotope Filter
	$filter.find('a').click(function(){
		var selector = $(this).attr('data-filter');

		try {
			$container.isotope({
				filter	: selector,
				animationOptions: {
					duration: 750,
					easing	: 'linear',
					queue	: false,
				}
			});
		} catch(err) {

		}
		return false;
	});


	var filterItemA	= $('.filter li a');

	filterItemA.on('click', function(){
		var $this = $(this);
		if ( !$this.hasClass('active')) {
			filterItemA.removeClass('active');
			$this.addClass('active');
		}
	});



	var $container2=$('.blog-isotope');
	try{
		$container2.imagesLoaded( function(){
			$container2.show();
			$container2.isotope({
				itemSelector: '.isotope-item'
			});

		});
	} catch(err) {
	}


	/*-------------------------------------------------*/
	/* =  fullwidth carousel
	 /*-------------------------------------------------*/
	try {
		$.browserSelector();
		// Adds window smooth scroll on chrome.
		if($("html").hasClass("chrome")) {
			$.smoothScroll();
		}
	} catch(err) {

	}

	/*-------------------------------------------------*/
	/* =  Scroll to TOP
	 /*-------------------------------------------------*/

	var animateTopButton = $('.go-top'),
		htmBody = $('html, body');

	animateTopButton.click(function(){
		htmBody.animate({scrollTop: 0}, 'slow');
		return false;
	});

	/*-------------------------------------------------*/
	/* =  remove animation in mobile device
	 /*-------------------------------------------------*/
	if ( winDow.width() < 992 ) {
		$('div.triggerAnimation').removeClass('animated');
		$('div.triggerAnimation').removeClass('triggerAnimation');
	}

	/*-------------------------------------------------*/
	/* =  flexslider
	 /*-------------------------------------------------*/
	try {

		var SliderPost = $('.flexpost');

		SliderPost.flexslider({
			animation: "fade",
			slideshowSpeed: 3000,
			easing: "swing"
		});
	} catch(err) {

	}



	$('.animation').waypoint(function(direction) {
		$(this).addClass('animation-active');
	}, { offset: '100%' });


	/* ---------------------------------------------------------------------- */
	/*	magnific-popup
	 /* ---------------------------------------------------------------------- */

	try {
		// Example with multiple objects
		$('.project-post .zoom, .zoomshort').magnificPopup({
			type: 'image',
			gallery: {
				enabled: true
			}
		});
	} catch(err) {

	}

	try {
		// Example with multiple objects
		$('.zoom.video').magnificPopup({
			type: 'iframe',
			gallery: {
				enabled: true
			}
		});
	} catch(err) {

	}

	/* ---------------------------------------------------------------------- */
	/*	Accordion
	 /* ---------------------------------------------------------------------- */
	var clickElem = $('a.accord-link');

	clickElem.on('click', function(e){
		e.preventDefault();

		var $this = $(this),
			parentCheck = $this.parents('.accord-elem'),
			accordItems = $('.accord-elem'),
			accordContent = $('.accord-content');

		if( !parentCheck.hasClass('active')) {

			accordContent.slideUp(400, function(){
				accordItems.removeClass('active');
			});
			parentCheck.find('.accord-content').slideDown(400, function(){
				parentCheck.addClass('active');
			});

		} else {

			accordContent.slideUp(400, function(){
				accordItems.removeClass('active');
			});

		}
	});

	/* ---------------------------------------------------------------------- */
	/*	Tabs
	 /* ---------------------------------------------------------------------- */
	var clickTab = $('.tab-links li a');

	clickTab.on('click', function(e){
		e.preventDefault();

		var $this = $(this),
			hisIndex = $this.parent('li').index(),
			tabCont = $('.tab-content'),
			tabContIndex = $(".tab-content:eq(" + hisIndex + ")");

		if( !$this.hasClass('active')) {

			clickTab.removeClass('active');
			$this.addClass('active');

			tabCont.slideUp(400);
			tabCont.removeClass('active');
			tabContIndex.delay(500).slideDown(400);
			tabContIndex.addClass('active');
		}
	});


	/*-------------------------------------------------*/
	/* = skills animate
	 /*-------------------------------------------------*/

	try{

		var skillBar = $('.skills-section');
		skillBar.appear(function() {

			var animateElement = $(".meter > span");
			animateElement.each(function() {
				$(this)
					.data("origWidth", $(this).width())
					.width(0)
					.animate({
						width: $(this).data("origWidth")
					}, 1200);
			});

		});
	} catch(err) {
	}

	/*-------------------------------------------------*/
	/* =  count increment
	 /*-------------------------------------------------*/
	try {
		$('.statistic-post').appear(function() {
			$('.timer').countTo({
				speed: 4000,
				refreshInterval: 60,
				formatter: function (value, options) {
					return value.toFixed(options.decimals);
				}
			});
		});
	} catch(err) {

	}

	/*-------------------------------------------------*/
	/* =  Shop accordion
	 /*-------------------------------------------------*/

	var AccordElement = $('a.accordion-link');

	AccordElement.on('click', function(e){
		e.preventDefault();
		var elemSlide = $(this).parent('li').find('.accordion-list-content');

		if( !$(this).hasClass('active') ) {
			$(this).addClass('active');
			elemSlide.slideDown(200);
		} else {
			$(this).removeClass('active');
			elemSlide.slideUp(200);
		}
	});

	/* ---------------------------------------------------------------------- */
	/*	menu responsive
	 /* ---------------------------------------------------------------------- */
	var menuClick = $('a.elemadded'),
		navbarVertical = $('.navbar-vertical');

	menuClick.on('click', function(e){
		e.preventDefault();

		if( navbarVertical.hasClass('active') ){
			navbarVertical.removeClass('active');
		} else {
			navbarVertical.addClass('active');
		}
	});

	/* Window bind resize if on mobile device*/
	if( !/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
		winDow.bind('resize', function(){
			navbarVertical.removeClass('active');
		});
	}

});

/*contact widget*/
function checkemail(emailaddress){
	"use strict";
	var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
	return pattern.test(emailaddress);
}
$(document).ready(function(){
	"use strict";
	$('#registerErrors, .widgetinfo').hide();
	var $messageshort = false;
	var $emailerror = false;
	var $nameshort = false;
	var $phoneshort = false;
	var $namelong = false;

	$('#contactFormWidget input#wformsend').click(function(){
		var $name = $('#wname').val();
		var $email = $('#wemail').val();
		var $phone = $('#wphone').val();
		var $message = $('#wmessage').val();
		var $contactemail = $('#wcontactemail').val();
		var $contacturl = $('#wcontacturl').val();
		var $subject = $('#wsubject').val();

		if ($name !== '' && $name.length < 3){ $nameshort = true; } else { $nameshort = false; }
		if ($name !== '' && $name.length > 30){ $namelong = true; } else { $namelong = false; }
		if ($phone !== '' && $phone.length < 3){ $phoneshort = true; } else { $phoneshort = false; }
		if ($email !== '' && checkemail($email)){ $emailerror = true; } else { $emailerror = false; }
		if ($message !== '' && $message !== 'Message' && $message.length < 3){ $messageshort = true; } else { $messageshort = false; }

		$('#contactFormWidget .loading').animate({opacity: 1}, 250);

		if ($name !== '' && $nameshort !== true && $namelong !== true && $phoneshort !== true && $email !== '' && $emailerror !== false && $message !== '' && $messageshort !== true && $contactemail !== ''){
			$.post($contacturl,
				{type: 'widget', contactemail: $contactemail, subject: $subject, name: $name, phone: $phone, email: $email, message: $message},
				function(/*data*/){
					$('#contactFormWidget .loading').animate({opacity: 0}, 250);
					$('.form').fadeOut();
					$('#bottom #wname, #bottom #wphone, #bottom #wemail, #bottom #wmessage').css({'border':'0'});
					$('.widgeterror').hide();
					$('.widgetinfo').fadeIn('slow');
					$('.widgetinfo').delay(2000).fadeOut(1000, function(){
						$('#wname, #wphone, #wemail, #wmessage').val('');
						$('.form').fadeIn('slow');
					});
				}
			);

			return false;
		} else {
			$('#contactFormWidget .loading').animate({opacity: 0}, 250);
			$('.widgeterror').hide();
			$('.widgeterror').fadeIn('fast');
			$('.widgeterror').delay(3000).fadeOut(1000);

			if ($name === '' || $name === 'Name' || $nameshort === true || $namelong === true){
				$('#wname').css({'border-left':'4px solid #red'});
			} else {
				$('#wname').css({'border-left':'4px solid #929DAC'});
			}

			if ($phone === '' || $phone === 'Phone' || $phoneshort === true){
				$('#wphone').css({'border-left':'4px solid #red'});
			} else {
				$('#wphone').css({'border-left':'4px solid #929DAC'});
			}

			if ($email === '' || $email === 'Email' || $emailerror === false){
				$('#wemail').css({'border-left':'4px solid red'});
			} else {
				$('#wemail').css({'border-left':'4px solid #929DAC'});
			}

			if ($message === '' || $message === 'Message' || $messageshort === true){
				$('#wmessage').css({'border-left':'4px solid red'});
			} else {
				$('#wmessage').css({'border-left':'4px solid #929DAC'});
			}

			return false;
		}
	});
});