<?php

/*****************************************************/
vc_map( array(
   "name" => __("Woocommerce products listing by special type", "trendystuff"),
   "base" => "shop_special_products",
   "class" => "",
   "icon" => "icon-wpb-my_shop_special_products",
   'admin_enqueue_css' => array(get_template_directory_uri().'/vc_templates/shortcodes.css'),
   "category" => __('Content', "trendystuff"),
   "params" => array(
       array(
         "type" => "dropdown",
         "class" => "",
         "heading" => __("Animation", "trendystuff"),
         "param_name" => "anim",
         "value" => array( "None"=>"", "bounce"=>"bounce", "flash"=>"flash", "pulse"=>"pulse", "shake"=>"shake", "swing"=>"swing", "tada"=>"tada", "wobble"=>"wobble", "bounceIn"=>"bounceIn", "bounceInDown"=>"bounceInDown", "bounceInLeft"=>"bounceInLeft", "bounceInRight"=>"bounceInRight", "bounceInUp"=>"bounceInUp", "bounceOut"=>"bounceOut", "bounceOutDown"=>"bounceOutDown", "bounceOutLeft"=>"bounceOutLeft", "bounceOutRight"=>"bounceOutRight", "bounceOutUp"=>"bounceOutUp", "fadeIn"=>"fadeIn", "fadeInDown"=>"fadeInDown", "fadeInDownBig"=>"fadeInDownBig", "fadeInLeft"=>"fadeInLeft", "fadeInLeftBig"=>"fadeInLeftBig", "fadeInRight"=>"fadeInRight", "fadeInRightBig"=>"fadeInRightBig", "fadeInUp"=>"fadeInUp", "fadeInUpBig"=>"fadeInUpBig", "fadeOut"=>"fadeOut", "fadeOutDown"=>"fadeOutDown", "fadeOutDownBig"=>"fadeOutDownBig", "fadeOutLeft"=>"fadeOutLeft", "fadeOutLeftBig"=>"fadeOutLeftBig", "fadeOutRight"=>"fadeOutRight", "fadeOutRightBig"=>"fadeOutRightBig", "fadeOutUp"=>"fadeOutUp", "fadeOutUpBig"=>"fadeOutUpBig", "flip"=>"flip", "flipInX"=>"flipInX", "flipInY"=>"flipInY", "flipOutX"=>"flipOutX", "flipOutY"=>"flipOutY", "lightSpeedIn"=>"lightSpeedIn", "lightSpeedOut"=>"lightSpeedOut", "rotateIn"=>"rotateIn", "rotateInDownLeft"=>"rotateInDownLeft", "rotateInDownRight"=>"rotateInDownRight", "rotateInUpLeft"=>"rotateInUpLeft", "rotateInUpRight"=>"rotateInUpRight", "rotateOut"=>"rotateOut", "rotateOutDownLeft"=>"rotateOutDownLeft", "rotateOutDownRight"=>"rotateOutDownRight", "rotateOutUpLeft"=>"rotateOutUpLeft", "rotateOutUpRight"=>"rotateOutUpRight",  "hinge"=>"hinge", "rollIn"=>"rollIn", "rollOut"=>"rollOut", "zoomIn"=>"zoomIn","zoomInDown"=>"zoomInDown", "zoomInLeft"=>"zoomInLeft","zoomInRight"=>"zoomInRight","zoomInUp"=>"zoomInUp","zoomOut"=>"zoomOut", "zoomOutLeft"=>"zoomOutLeft", "zoomOutRight"=>"zoomOutRight","zoomOutUp"=>"zoomOutUp"),
         "description" => __(" Animation.", "trendystuff")
      ),
	   array(
         "type" => "dropdown", 
         "class" => "",
         "heading" => __("Listing type", "trendystuff"),
         "param_name" => "type",
         "value"=>array("On Sale"=>"1", "Best selling"=>"2", "Featured"=>"3")
      ),
	    array(
         "type" => "textfield",
         "class" => "",
         "heading" => __("Items limit", "trendystuff"),
         "param_name" => "limit",
		 "value"=>"12"
      ),
	   array(
         "type" => "textfield",
         "class" => "",
         "heading" => __("Items to Show", "trendystuff"),
         "param_name" => "items",
		 "value"=>"4"
      ),
	    array(
         "type" => "dropdown", 
         "class" => "",
         "heading" => __("Automatic sliding", "trendystuff"),
         "param_name" => "automatic",
         "value"=>array("No"=>"false", "Yes"=>"true")
      ),
	    array(
         "type" => "textfield",
         "class" => "",
         "heading" => __("Sliding interval", "trendystuff"),
         "param_name" => "interval",
		 "value"=>"2500"
      ),
	    array(
         "type" => "dropdown", 
         "class" => "",
         "heading" => __("Show arrows", "trendystuff"),
         "param_name" => "showarrows",
         "value"=>array("Yes"=>"true", "No"=>"false")
      ),
	    array(
         "type" => "dropdown", 
         "class" => "",
         "heading" => __("Order", "trendystuff"),
         "param_name" => "order",
         "value"=>array("Descending"=>"DESC", "Ascending"=>"ASC")
      ),
	  array(
         "type" => "dropdown", 
         "class" => "",
         "heading" => __("Order by", "trendystuff"),
         "param_name" => "orderby",
         "value"=>array("Default sorting"=>"menu_order", "Sort by popularity"=>"popularity", "Sort by average rating"=>"rating", "Sort by newness"=>"date", "Sort by price: low to high"=>"price", "Sort by price: high to low"=>"price-desc")
      ),
        array(
         "type" => "textfield",
         "class" => "",
         "heading" => __("Extra class", "trendystuff"),
         "param_name" => "class",
         "description" => __(' Extra class name', "trendystuff")
      )
   )
) );
?>